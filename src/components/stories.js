import List from "./List";

const initialStories = [
    {
        title: 'React',
        url: 'https://reactjs.org/',
        authors: ['Jordan Walke'],
        num_comments: 3,
        points: 4,
        objectID: 0,
    },
    {
        title: 'Redux',
        url: 'https://redux.js.org/',
        authors: ['Dan Abramov', 'Andrew Clark'],
        num_comments: 2,
        points: 5,
        objectID: 1,
    },
    {
        title: 'React',
        url: 'https://reactjs.org/',
        authors: ['Jordan Walke'],
        num_comments: 3,
        points: 4,
        objectID: 2,
    },
    {
        title: 'Redux',
        url: 'https://redux.js.org/',
        authors: ['Dan Abramov', 'Andrew Clark'],
        num_comments: 2,
        points: 5,
        objectID: 3,
    },
    {
        title: 'React',
        url: 'https://reactjs.org/',
        authors: ['Jordan Walke'],
        num_comments: 3,
        points: 4,
        objectID: 4,
    },
    {
        title: 'Redux',
        url: 'https://redux.js.org/',
        authors: ['Dan Abramov', 'Andrew Clark'],
        num_comments: 2,
        points: 5,
        objectID: 5,
    },
];

//const getAsyncStories = () => Promise.resolve({ data: { stories: initialStories } });

const getAsyncStories = () => new Promise((resolve) => {
    setTimeout(() => {
        resolve({ data: { stories: initialStories } });
    }, 500);
});

const withError = (promise, probError = 0.1) => { //Probabilidad de error 0.1 (Por Ejemplo)
    const isError = Math.random() <= probError;

    return promise.then(result => {
        if(isError){
            throw new Error('Error');
        }
        return result;
    })
}

export { getAsyncStories, withError };