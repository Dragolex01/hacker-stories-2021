import styled from 'styled-components';
import './Item.scss';

    const pointsStyle = {
        color: 'red',
        fontSize: '20px'
    };

    const Comments = styled.span`
        font-size: 1.5rem;
        text-align: center;
        color: palevioletred;
    `;

export default function Item({url, title, authors, num_comments, points, style = {}, handleDelete }) {

    return (
        <div style={style}>
            <span style={{ width: '40%' }}>
                <a href={url}>{title}</a>
            </span>
            {/*<ul>
                    {
                        //item.authors.map(author => <li key={author}>{author}</li>)
                        item.authors.map(function(author){
                        return(
                            <li key={author}>{author}</li>
                        )
                        })
                    }
                    </ul>*/}
            <span className="item-author" style={{ width: '30%' }}>{authors}</span>
            <Comments style={{ width: '10%' }}>{num_comments}</Comments>
            <span style={{ ...pointsStyle, width: '10%' }}>{points}</span>
            <span style={{ width: '10%' }}>
                <button onClick={handleDelete}>delete</button>
            </span>
        </div>
    )
}