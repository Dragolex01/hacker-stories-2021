import { useState, useEffect, useReducer, useRef } from 'react';
import { Outlet, Link } from 'react-router-dom';
import List from './List';
import Search from './Search';
import Example from './Example';
import Counter from './Counter2';
import useSemiPersistentState from '../useSemiPersistentState';

const API_ENDPOINT = 'https://hn.algolia.com/api/v1/search?query=';

const initialStoriesState = {
  loading: false,
  error: false,
  stories: []
}

const storiesReducer = (state, action) => {
  switch(action.type){
    case "STORIES_FETCH_INIT":
      return {
        loading: true,
        error: false,
        stories: []
      }
    case "STORIES_FETCH_SUCCESS":
      return{
        loading: false,
        error: false,
        stories: action.payload
      }
    case "STORIES_FETCH_FAILURE":
      return{
        loading: false,
        error: true,
        stories: []
      }
    case "STORIES_DELETE":
    return{
      ...state, //El estado anterior
      stories: state.stories.filter(story => story.objectID !== action.payload)
    }
    default:
      throw Error("Action Stories Error")
  }

  // if(action.type === 'SET_STORIES'){
  //   return action.payload;
  // }else{
  //   throw new Error();
  // }
}

function App() {

  //const [searchTerm, setSearchTerm] = useState(sessionStorage.getItem('search') != null ? sessionStorage.getItem('search') : 'React'); //Al actualizar la página cogera el valor del 'LocalStorge' (última búsqueda).
  const [searchTerm, setSearchTerm] = useSemiPersistentState('search', 'React');
  const lastSearchVersion = useRef(0);
  const [searchVersion, setSearchVersion] = useState(1);
  const [page, setPage] = useState(0);
  
  useEffect(() => {
    loadStories(searchTerm, page);
    setSearchVersion(searchVersion => searchVersion + 1);
    lastSearchVersion.current = lastSearchVersion.current + 1;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchTerm, page]);

  useEffect(() => {
    setPage(0);
  }, [searchTerm])

  ///const {stories, loading, error} = storiesState;
  const [storiesState, dispatchStories] = useReducer(
    storiesReducer,
    initialStoriesState
  );

  async function loadStories(searchTerm, page){
    dispatchStories({ type: 'STORIES_FETCH_INIT' });

    try{
      const response = await fetch(`${API_ENDPOINT}${searchTerm}&page=${page}`) //Metodo fetch --> coje el API y le pasa el termino de búsqueda
      const result = await response.json() //Obtengo una respuesta (Un json)
      
      if(searchVersion === lastSearchVersion.current){
          dispatchStories({
            type: 'STORIES_FETCH_SUCCESS',
            payload: result.hits
          });
      }
    } catch (error){
      dispatchStories({ type: 'STORIES_FETCH_FAILURE' })
    }
  }

  function showMore(){
    setPage(page + 1)
  }

  // function loadStories(searchTerm){
  //   dispatchStories({ type: 'STORIES_FETCH_INIT' });
  //   fetch(`${API_ENDPOINT}${searchTerm}`) //Metodo fetch --> coje el API y le pasa el termino de búsqueda
  //     .then((response) => response.json()) //Obtengo una respuesta (Un json)
  //     .then((result) => { //El json recibido tambien tiene una promesa
  //       if(searchVersion === lastSearchVersion.current){
  //         dispatchStories({
  //           type: 'STORIES_FETCH_SUCCESS',
  //           payload: result.hits
  //         });
  //       }
  //     })
  //     .catch(() =>
  //       dispatchStories({ type: 'STORIES_FETCH_FAILURE' })
  //     );
  // }

/*
  function loadStories(){
    // setIsError(false);
    // setIsLoading(true);
    dispatchStories({
      type: "STORIES_FETCH_INIT"
    })
    withError(getAsyncStories()).then(result => {
      dispatchStories({
        type: "STORIES_FETCH_SUCCESS",
        payload: result.data.stories
      });
      // setList(result.data.stories);
      // setIsLoading(false);
    }).catch(_err => dispatchStories({
      type: "STORIES_FETCH_ERROR"
    }));
  }
*/

  function onDelete(objectID){
    return function() {
      dispatchStories({
        type: "STORIES_DELETE",
        payload: objectID
      })
    }
  }

  return (
    <div>
      <h1>My Hacker Stories</h1>
      <Example />
      <Counter />
      <Search searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
      <hr />
      <List searchTerm={searchTerm} {...storiesState} loadStories={loadStories} onDelete={onDelete}/>
      <button onClick={showMore}>Show more</button>
      <nav style={{
        borderBottom: 'Solid 1px',
        paddingBottom: '1rem'
      }}>
        <Link to='/invoices'>Invoices</Link> |{" "}
        <Link to='/expenses'>Expenses</Link>
      </nav>
      <Outlet />
    </div>
  );
}

export default App;


/*
-LocalStorage --> no tiene tiempo se caducidad.
-SessionStorage --> se elimina cuando vuelves a abrir la página, no al refrescar.
*/