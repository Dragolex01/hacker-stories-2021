import { debounce } from "../utils";
import InputWithLabel from "./InputWithLabel";

export default function Search({ searchTerm, setSearchTerm }) {

  function handleChange(event){
    setSearchTerm(event.target.value);
    //sessionStorage.setItem('search', event.target.value); //Guardar la última búsqueda en el almacenamiento de la página
  }

  return (
    <>
      {/*
      <label htmlFor="search">Search: </label>
      <input id="search" type="text" onChange={handleChange} value={searchTerm} />
      */}

      <InputWithLabel id='search' isFocused={true} onInputChange={debounce(handleChange)} >
        <strong>Search:</strong>
      </InputWithLabel>

      <p>
        Searching for <strong>{searchTerm}</strong>.
      </p>

    </>
  );
}
