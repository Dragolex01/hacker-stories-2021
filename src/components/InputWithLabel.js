import { useEffect, useRef } from "react";

const InputWithLabel = ({ id, type='text', value, defaultValue,  onInputChange, isFocused=false, children }) => {

    const inputRef = useRef();
 
    //Forma imperativa de darle el foco

    useEffect(() => {
        if(isFocused && inputRef.current){ //Si quieres el foco y el input existe
            inputRef.current.focus(); //Se pone el foco
        }
    }, [isFocused]);

    return(
        <>
            <label htmlFor={id}>{children}</label>
            &nbsp;
            <input id={id} ref={inputRef} type={type} value={value} defaultValue={defaultValue} onChange={onInputChange} />
        </>
    );
}

/*

//Forma no imperativa de darle el foco

const InputWithLabel = ({ id, type='text', value, onInputChange, isFocused=false, children }) => (
    <>
        <label htmlFor={id}>{children}</label>
        &nbsp;
        <input id={id} type={type} value={value} onChange={onInputChange} autoFocus={isFocused} />
    </>
)
*/

export default InputWithLabel;