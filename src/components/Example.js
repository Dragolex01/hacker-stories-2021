import React, { useState ,useEffect } from "react";

function Example(){

    const [windowWidthSize, setWindowWidthSize] = useState(0);
    const [show, setShow] = useState(true);

    useEffect(() => {
        function handleResize(e){

            //console.log('resize');
            const { width } = document.body.getBoundingClientRect();

            setWindowWidthSize(Math.ceil(width));
        }
        handleResize()

        if(show){
            window.addEventListener('resize', handleResize)
        }

        return () => {
            window.removeEventListener('resize', handleResize)
        }

    }, [show]);

    return (
        <h1>
            {show && 'The window size ' + windowWidthSize + ' pixels'}
            <button onClick={() => setShow(!show)}>{show ? 'hide' : 'show'}</button>
        </h1>
    );
}

export default Example;