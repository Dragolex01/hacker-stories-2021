import React, { useEffect, useState, useRef } from 'react';

function Counter(){
    const [state, setState] = useState(1);
    const ref = useRef(state)

    useEffect(() => {
        setTimeout(() => {
            setState(state + 1);
            ref.current = ref.current + 1;
        }, 1000);

        setTimeout(() => {
            //console.log(">>>>>>>", state);
            //console.log("++++++", ref.current);            
        }, 2000);

        //return () => clearInterval(id);
    }, [state]);

    return (
        <>
            <h1>{state}</h1>
        </>
    )
}

/*
function Counter(){
    const [count, setCount] = useState(0);
    const [step, setStep] = useState(1);
   
    useEffect(() => {
        const id = setInterval(() => {
            setCount(c => c + step);
        }, 1000);
        return () => clearInterval(id);
    }, [step]);

    return (
        <>
            <h1>{count}</h1>
            <input value={step} onChange={e => setStep(Number(e.target.value))} />
        </>
    );
}
*/

export default Counter;

/*
function Counter({name = 'Peter'}){
    const [count, setCount] = useState(0);
    const latestCount = useRef(count);

    useEffect(() => {
        latestCount.current = count; //Cambiar valor de latestCount con 'current'
        setTimeout(() => {
            console.log(`You clicked ${latestCount.current} times`);
        }, 3000);
    });

    useEffect(() => {
        document.title = 'Hello, ' + name;
    }, [name]);

    return (
        <div>
            <p>You clicked {count} times</p>
            <button onClick={() => setCount(count + 1)}>Click me</button>
        </div>
    );
}
*/
