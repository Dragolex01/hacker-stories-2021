import { useState } from 'react';
import Item from "./Item";
import { sortBy } from '../utils';

const SORTS = {
  NONE: (list) => list,
  TITLE: (list, isReverse) => sortBy(list, 'title', isReverse),
  AUTHOR: (list, isReverse) => sortBy(list, 'author', isReverse),
  COMMENT: (list, isReverse) => sortBy(list, 'num_comments', isReverse).reverse(),
  POINT: (list, isReverse) => sortBy(list, 'points', isReverse).reverse()
}

const colors = ['red', 'green', 'pink'];

function List({ searchTerm, stories, loading, error, loadStories, onDelete }) {

  // const [list, setList] = useState([]);
  // const [isLoading, setIsLoading] = useState(false);
  // const [isError, setIsError] = useState(false);

  const [sort, setSort] = useState({
    sortKey: 'NONE',
    isReverse: false
  });
  const sortFunction = SORTS[sort.sortKey];
  const sortedList = sortFunction(stories, sort.isReverse);

  const handleShort = (sortKey) => { //SortKey --> clave de ordenación
    setSort({
      sortKey,
      isReverse: sortKey === sort.sortKey ? !sort.isReverse : false
    })
  }

  if(error){
    return(
      <>
        <p>Error!</p>
        <button onClick={() => loadStories(searchTerm)}>Retry</button>
      </>
      ) 
  }

  return (
    loading ? 'loading...' : <ul>
      <li style={{ display: 'flex' }}>
        <span style={{ width: '40%' }}>
          <button type='button' onClick={() => handleShort('TITLE')}>Title</button>
        </span>
        <span style={{ width: '30%' }}>
          <button type='button' onClick={() => handleShort('AUTHOR')}>Author</button>
        </span>
        <span style={{ width: '10%' }}>
          <button type='button' onClick={() => handleShort('COMMENT')}>Comments</button>
        </span>
        <span style={{ width: '10%' }}>
          <button type='button' onClick={() => handleShort('POINT')}>Points</button>
        </span>
        <span style={{ width: '10%' }}>Actions</span>
      </li>
      {
        sortedList.filter(item => item.title && item.title.toLowerCase().includes(searchTerm.toLowerCase())).map(function (item, i) {
          return (
            <li key={item.objectID}>
               <Item {...item} style={{ backgroundColor: colors[i % colors.length], display: 'flex' }} handleDelete={onDelete(item.objectID)} /> {/* Vuelve a empezar cuando i entre 3 es 0 */}
            </li>
          );
        })       
      }
    </ul>
  );
}

export default List;
