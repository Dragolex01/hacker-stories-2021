import React, { useEffect, useReducer } from 'react';

const initialState = {
    count: 0,
    step: 1
}

function reducer(state, action){
    const {count, step} = state;

    if(action.type === 'tick'){
        return {count: count + step, step};
    }else if(action.type === 'step'){
        return {count, step: action.step};
    }else{
        throw new Error();
    }
}

function Counter(){
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        const id = setInterval(() => {
            dispatch({type: 'tick'});
        }, 1000);
        return () => clearInterval(id);
    }, [dispatch]);

    return (
        <>
            <h1>{state.count}</h1>
            <input type='text' value={state.step} onChange={(e) => dispatch({type: 'step', step: Number(e.target.value)})} />
        </>
    )
}

/*
function Counter(){
    const [count, setCount] = useState(0);
    const [step, setStep] = useState(1);
   
    useEffect(() => {
        const id = setInterval(() => {
            setCount(c => c + step);
        }, 1000);
        return () => clearInterval(id);
    }, [step]);

    return (
        <>
            <h1>{count}</h1>
            <input value={step} onChange={e => setStep(Number(e.target.value))} />
        </>
    );
}
*/

export default Counter;

/*
function Counter({name = 'Peter'}){
    const [count, setCount] = useState(0);
    const latestCount = useRef(count);

    useEffect(() => {
        latestCount.current = count; //Cambiar valor de latestCount con 'current'
        setTimeout(() => {
            console.log(`You clicked ${latestCount.current} times`);
        }, 3000);
    });

    useEffect(() => {
        document.title = 'Hello, ' + name;
    }, [name]);

    return (
        <div>
            <p>You clicked {count} times</p>
            <button onClick={() => setCount(count + 1)}>Click me</button>
        </div>
    );
}
*/
