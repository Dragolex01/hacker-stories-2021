import { useState, useEffect } from 'react';

const useSemiPersistentState = (key, initialState) => {
    const [value, setValue] = useState(
        sessionStorage.getItem(key) || initialState
    );

    useEffect(() => {
        sessionStorage.setItem(key, value);
    }, [value, key]);

    return [value, setValue];
}

/*
const useSemiPersistentState = (initialState) => {
    const [searchTerm, setSearchTerm] = useState(
        sessionStorage.getItem('search') || initialState
    );

    useEffect(() => {
        sessionStorage.setItem('search', searchTerm);
    }, [searchTerm]);

    return [searchTerm, setSearchTerm];
}
*/

export default useSemiPersistentState;