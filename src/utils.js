export function debounce(f, timeout = 300){
    let id = 0;

    return function(...args){
        if(id){
            clearTimeout(id);
        }
        id = setTimeout(() => {
            return f(...args);
        },timeout)   
    }
}

export function sortBy(list, key, isReverse){
    const sortedList = list.slice().sort((item1, item2) => {

        if(typeof (item1[key]) === "number"){
            return (item1[key] || 0) - (item2[key] || 0)
        }

        if(item1[key].toLowerCase() < item2[key].toLowerCase()){
            return -1;
        }else if(item1[key].toLowerCase() > item2[key].toLowerCase()){
            return +1;
        }else{
            return 0;
        }
    })
    
    return isReverse ? sortedList.reverse() : sortedList
}

// function echo (x){
//     console.log(x);
// }

// const debounceEcho = debounce(echo, 2000);

// debounceEcho("Hola");
// debounceEcho("Adios");